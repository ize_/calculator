﻿using System;

namespace Test01
{
    class Program
    {
        static void Main(string[] args)
        {
            //Declaracion de las variable a usar
            int num1 = 0;
            int num2 = 0;

            //Declaracion del titulo de la aplicacion
            Console.WriteLine("Calculadora en C#");
            Console.WriteLine("-----------------------\n");

            //Preguntar por el primer numero
            Console.WriteLine("Escriba un numero, presione Enter");
            num1 = Convert.ToInt32(Console.ReadLine());

            //Preguntar por el sugundo numero
            Console.WriteLine("Escriba otro numero, presione Enter");
            num2 = Convert.ToInt32(Console.ReadLine());

            //Preguntar que operacion va a realizar
            Console.WriteLine("Elije una operacion de la siguiente lista");
            Console.WriteLine("\ts - Suma");
            Console.WriteLine("\tr - Resta");
            Console.WriteLine("\tm - Multiplica");
            Console.WriteLine("\td - Division");
            Console.WriteLine("Que operacion va a realizar?...");

            //Casos para la eleccion de las operaciones
            switch (Console.ReadLine())
            {
                case "s":
                    Console.WriteLine($"Tu resulatado es: {num1} + {num2} = " + (num1 + num2));
                    break;
                case "r":
                    Console.WriteLine($"Tu resulatado es: {num1} - {num2} = " + (num1 - num2));
                    break;
                case "m":
                    Console.WriteLine($"Tu resulatado es: {num1} * {num2} = " + (num1 * num2));
                    break;
                case "d":
                    Console.WriteLine($"Tu resulatado es: {num1} / {num2} = " + (num1 / num2));
                    break;
            }
            //Cerrar porgrama
            Console.Write("Presiona una tecla para salir ...");
            Console.ReadKey();

        }
    }
}
